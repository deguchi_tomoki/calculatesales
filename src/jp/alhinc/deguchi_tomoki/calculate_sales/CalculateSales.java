package jp.alhinc.deguchi_tomoki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました"); //コマンドライン引数が渡されているか確認
			return;
		}

		Map<String, String> branchNames = new HashMap<>(); //支店コード-支店名のmap作成
		Map<String, Long> branchSales = new HashMap<>(); //支店コード-売上額のmapを作成
		Map<String, String> commodityNames = new HashMap<>(); //商品コード-商品名のmapを作成
		Map<String, Long> commoditySales = new HashMap<>();//商品コード-売上額のmap作成

		//支店定義ファイルの読み込み

		if (!inputFile(args[0], "branch.lst", branchNames, branchSales, "[0-9]{3}$", "支店")) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}

		//商品定義ファイルの読み込み

		if (!inputFile(args[0], "commodity.lst", commodityNames, commoditySales, "^[A-Za-z0-9]{8}$", "商品")) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}

		//売上ファイル読み込み、支店別,商品別合計値集計

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for (int i = 0; i < files.length; i++) {
			String saleFileName = files[i].getName();
			if (files[i].isFile() && saleFileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		Collections.sort(rcdFiles);
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません。");
				return;
			}
		}
		BufferedReader br = null;
		for (int i = 0; i < rcdFiles.size(); i++) {
			List<String> fileDatas = new ArrayList<>();
			try {
				File saleFiles = rcdFiles.get(i);
				FileReader fr = new FileReader(saleFiles);
				br = new BufferedReader(fr);
				String lineSale;
				while ((lineSale = br.readLine()) != null) {
					fileDatas.add(lineSale);
				}
				if (fileDatas.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です。");
					return;
				}
				if (!branchNames.containsKey(fileDatas.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です。");
					return;
				}
				if (!commodityNames.containsKey(fileDatas.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}
				if (!fileDatas.get(2).matches("^[0-9]\\d*$")) {
					System.out.println("予期せぬエラーが発生しました。"); //売上金額が数字か確認
					return;
				}
				long fileSale = Long.parseLong(fileDatas.get(2));
				Long branchSaleAmount = branchSales.get(fileDatas.get(0)) + fileSale;
				Long commoditySaleAmount = commoditySales.get(fileDatas.get(1)) + fileSale;
				if (branchSaleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました。");
					return;
				}
				branchSales.put(fileDatas.get(0), branchSaleAmount);
				commoditySales.put(fileDatas.get(1), commoditySaleAmount);
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました。");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました。");
						return;
					}
				}
			}
		}

		//支店別集計ファイル出力

		if (!outputFile(args[0], "branch.out", branchNames, branchSales)) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}

		//商品別集計ファイルの出力

		if (!outputFile(args[0], "commodity.out", commodityNames, commoditySales)) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}
	}

	//定義ファイル読込メソッド

	private static boolean inputFile(String pathName, String fileName, Map<String, String> names,
			Map<String, Long> sales, String regularExpression, String type) {
		BufferedReader br = null;
		try {
			File file = new File(pathName, fileName);
			if (!file.exists()) {
				System.out.println(type + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				String[] items = line.split(",");
				if ((items.length != 2) || (!items[0].matches(regularExpression))) {
					System.out.println(type + "定義ファイルのフォーマットが不正です");
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}

	//集計ファイル出力メソッド

	private static boolean outputFile(String pathName, String FileName, Map<String, String> names,
			Map<String, Long> sales) {
		BufferedWriter Bw = null;
		try {
			File TotalFile = new File(pathName, FileName);
			FileWriter Fw = new FileWriter(TotalFile);
			Bw = new BufferedWriter(Fw);
			for (String key : sales.keySet()) {
				Bw.write(key + "," + names.get(key) + "," + sales.get(key));
				Bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if (Bw != null) {
				try {
					Bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}
}
